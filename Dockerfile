# Usa una imagen base (en este caso, una imagen de Python)
FROM python:3.8-slim

# Establece el directorio de trabajo dentro del contenedor
WORKDIR /app

# Copia el archivo de la aplicación al directorio de trabajo en el contenedor
COPY hello.py /app

# Comando para ejecutar la aplicación cuando se inicie el contenedor
CMD ["python", "hello.py"]
